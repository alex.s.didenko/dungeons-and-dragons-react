
import React from "react"
import ReactDOM from 'react-dom/client';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import * as ReactDOMClient from 'react-dom/client';
import './css/style.css'
import Main from "./Main";

const client = new QueryClient();

const root = ReactDOM.createRoot( 
    document.getElementById('root')
  );
  root.render(
      <QueryClientProvider client={client}><Main/></QueryClientProvider>
  );
